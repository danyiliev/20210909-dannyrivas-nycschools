//
//  SchoolTableViewCell.swift
//  20210909-DannyRivas-NYCSchools
//
//  Created by Danny on 9/10/21.
//

import UIKit

class SchoolTableViewCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    
    func configure(_ school: School) {
        nameLabel.text = school.name
        addressLabel.text = [school.addressLine1, school.city, school.zip].map({$0}).joined(separator: ", ")
    }
}
