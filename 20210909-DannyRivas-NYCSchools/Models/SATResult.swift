//
//  SATResult.swift
//  20210909-DannyRivas-NYCSchools
//
//  Created by Danny on 9/9/21.
//

import Foundation

struct SATResult {
    var dbn: String
    var name: String
    var numOfSatTestTakers: String
    var satCriticalReadingAvgScore: String
    var satMathAvgScore: String
    var satWritingAvgScore: String
}


extension SATResult : Codable {
    enum SchoolDecodingKeys: String, CodingKey {
        case dbn = "dbn"
        case name = "school_name"
        case numOfSatTestTakers = "num_of_sat_test_takers"
        case satCriticalReadingAvgScore = "sat_critical_reading_avg_score"
        case satMathAvgScore = "sat_math_avg_score"
        case satWritingAvgScore = "sat_writing_avg_score"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: SchoolDecodingKeys.self)
        
        dbn = try container.decode(String.self, forKey: .dbn)
        name = try container.decode(String.self, forKey: .name)
        numOfSatTestTakers = try container.decode(String.self, forKey: .numOfSatTestTakers)
        satCriticalReadingAvgScore = try container.decode(String.self, forKey: .satCriticalReadingAvgScore)
        satMathAvgScore = try container.decode(String.self, forKey: .satMathAvgScore)
        satWritingAvgScore = try container.decode(String.self, forKey: .satWritingAvgScore)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: SchoolDecodingKeys.self)
        
        try container.encode(dbn, forKey: .dbn)
        try container.encode(name, forKey: .name)
        try container.encode(numOfSatTestTakers, forKey: .numOfSatTestTakers)
        try container.encode(satCriticalReadingAvgScore, forKey: .satCriticalReadingAvgScore)
        try container.encode(satMathAvgScore, forKey: .satMathAvgScore)
        try container.encode(satWritingAvgScore, forKey: .satWritingAvgScore)
    }
}
