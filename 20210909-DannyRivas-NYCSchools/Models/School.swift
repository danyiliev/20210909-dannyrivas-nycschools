//
//  School.swift
//  20210909-DannyRivas-NYCSchools
//
//  Created by Danny on 9/9/21.
//

import Foundation

struct School {
    var dbn: String
    var name: String
    var overview: String
    var location: String
    var phone: String
    var email: String?
    var fax: String?
    var website: String?
    var totalStudents: String?
    
    var addressLine1: String
    var city: String
    var zip: String
    var stateCode: String
    var latitude: String?
    var longitude: String?
}


extension School : Codable {
    enum SchoolDecodingKeys: String, CodingKey {
        case dbn = "dbn"
        case name = "school_name"
        case overview = "overview_paragraph"
        case location
        case phone = "phone_number"
        case fax = "fax_number"
        case email = "school_email"
        case website
        case totalStudents = "total_students"
        case addressLine1 = "primary_address_line_1"
        case city
        case zip
        case stateCode = "state_code"
        case latitude
        case longitude
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: SchoolDecodingKeys.self)
        
        dbn = try container.decode(String.self, forKey: .dbn)
        name = try container.decode(String.self, forKey: .name)
        overview = try container.decode(String.self, forKey: .overview)
        location = try container.decode(String.self, forKey: .location)
        phone = try container.decode(String.self, forKey: .phone)
        fax = try container.decodeIfPresent(String.self, forKey: .fax)
        email = try container.decodeIfPresent(String.self, forKey: .email)
        website = try container.decodeIfPresent(String.self, forKey: .website)
        totalStudents = try container.decodeIfPresent(String.self, forKey: .totalStudents)
        addressLine1 = try container.decode(String.self, forKey: .addressLine1)
        city = try container.decode(String.self, forKey: .city)
        zip = try container.decode(String.self, forKey: .zip)
        stateCode = try container.decode(String.self, forKey: .stateCode)
        latitude = try container.decodeIfPresent(String.self, forKey: .latitude)
        longitude = try container.decodeIfPresent(String.self, forKey: .longitude)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: SchoolDecodingKeys.self)
        
        try container.encode(dbn, forKey: .dbn)
        try container.encode(name, forKey: .name)
        try container.encode(overview, forKey: .overview)
        try container.encode(location, forKey: .location)
        try container.encode(phone, forKey: .phone)
        try container.encodeIfPresent(fax, forKey: .fax)
        try container.encodeIfPresent(email, forKey: .email)
        try container.encodeIfPresent(website, forKey: .website)
        try container.encode(addressLine1, forKey: .addressLine1)
        try container.encode(city, forKey: .city)
        try container.encode(zip, forKey: .zip)
        try container.encode(stateCode, forKey: .stateCode)
        
        try container.encodeIfPresent(totalStudents, forKey: .totalStudents)
        try container.encodeIfPresent(latitude, forKey: .latitude)
        try container.encodeIfPresent(longitude, forKey: .longitude)
        
    }
}
