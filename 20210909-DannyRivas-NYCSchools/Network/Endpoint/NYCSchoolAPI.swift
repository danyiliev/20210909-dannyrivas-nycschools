//
//  NYCSchoolAPI.swift
//  20210909-DannyRivas-NYCSchools
//
//  Created by Danny on 9/9/21.
//

import Foundation

public enum NYCSchoolAPI {
    case getSchools
    
    // Note: number of api request is limited per token. Prefer use fetch all once
    case getSATResult(dbn: String)
    case getSATResults
}

extension NYCSchoolAPI: EndPointType {
    var baseURL: URL {
        return URL(string: "https://data.cityofnewyork.us/resource")!
    }
    
    var path: String {
        switch self {
        case .getSchools:
            return "/s3k6-pzi2.json"
        case .getSATResult(let dbn):
            return "/f9bf-2cp4.json?dbn=\(dbn)"
        case .getSATResults:
            return "/f9bf-2cp4.json"
        }
    }
    
    var httpMethod: HTTPMethod {
        switch self {
        case .getSchools, .getSATResult, .getSATResults:
            return .get
        }
    }
    
    var task: HTTPTask {
        switch self {
        case .getSchools, .getSATResult, .getSATResults:
            return .requestParametersAndHeaders(bodyParameters: nil, bodyEncoding: .urlAndJsonEncoding, urlParameters: nil, additionHeaders: headers)
        }
    }
    
    var headers: HTTPHeaders? {
        switch self {
        case .getSchools, .getSATResults, .getSATResult:
            return ["X-App-Token": NYCSchoolAppToken]
        }
    }
}
