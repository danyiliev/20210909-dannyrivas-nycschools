//
//  HTTPMethod.swift
//  20210909-DannyRivas-NYCSchools
//
//  Created by Danny on 9/9/21.
//

import Foundation

public enum HTTPMethod: String {
    case get    = "GET"
    case post   = "POST"
    case put    = "PUT"
    case patch  = "PATCH"
    case delete = "DELETE"
}
