//
//  HTTPTask.swift
//  20210909-DannyRivas-NYCSchools
//
//  Created by Danny on 9/9/21.
//

import Foundation

public typealias HTTPHeaders = [String : String]

/// Responsible for configuring parameters for a specific endPoint.
public enum HTTPTask {
    case request
    
    case requestParameters(bodyParameters: Parameters?,
        bodyEncoding: ParameterEncoding,
        urlParameters: Parameters?)
    
    case requestParametersAndHeaders(bodyParameters: Parameters?,
        bodyEncoding: ParameterEncoding,
        urlParameters: Parameters?,
        additionHeaders: HTTPHeaders?)
    case requestOctetStreamParametersAndHeaders(bodyParameters: Data,
        bodyEncoding: ParameterEncoding,
        urlParameters: Parameters?,
        additionHeaders: HTTPHeaders?)
}
