//
//  NetworkManager.swift
//  20210909-DannyRivas-NYCSchools
//
//  Created by Danny on 9/9/21.
//

import Foundation

enum Result<String> {
    case success
    case failure(String)
}

struct NetworkManager {
    let router = Router<NYCSchoolAPI>()
    
    // MARK: - NYC Schools
    
    /// Get all school information
    ///
    /// - Parameter completion: school array or error string
    func getSchools(completion: @escaping (_ schools: [School]?,_ error: String?) -> ()) {
        
        router.request(.getSchools) { data, response, error in
            guard error == nil else {
                completion(nil, "Please check your network connection.")
                return
            }
            
            if let response = response as? HTTPURLResponse {
                let result = NetworkResponseHandler.handleNetworkResponse(response)
                switch result {
                case .success:
                    guard let responseData = data else {
                        completion(nil, NetworkResponse.noData.rawValue)
                        return
                    }
                    
                    do {
                        let schools = try JSONDecoder().decode([School].self, from: responseData)
                        completion(schools, nil)
                    } catch {
                        #if DEBUG
                        print("GET Schools: ERROR \n \(error)")
                        #endif
                        completion(nil, NetworkResponse.unableToDecode.rawValue)
                    }
                case .failure(let networkFailureError):
                    completion(nil, networkFailureError)
                }
            }
        }
    }
    
    /// Get a school SAT Result
    ///
    /// - Parameter completion: sat result or error string
    func getSATResult(dbn: String, completion: @escaping (_ result: SATResult?,_ error: String?) -> ()) {
        
        router.request(.getSATResult(dbn: dbn)) { data, response, error in
            guard error == nil else {
                completion(nil, "Please check your network connection.")
                return
            }
            
            if let response = response as? HTTPURLResponse {
                let result = NetworkResponseHandler.handleNetworkResponse(response)
                switch result {
                case .success:
                    guard let responseData = data else {
                        completion(nil, NetworkResponse.noData.rawValue)
                        return
                    }
                    
                    do {
                        let schools = try JSONDecoder().decode([SATResult].self, from: responseData)
                        
                        if let school = schools.first {
                            completion(school, nil)
                        } else {
                            completion(nil, NetworkResponse.noData.rawValue)
                        }
                        
                    } catch {
                        #if DEBUG
                        print("GET SAT Result: ERROR \n \(error)")
                        #endif
                        completion(nil, NetworkResponse.unableToDecode.rawValue)
                    }
                case .failure(let networkFailureError):
                    completion(nil, networkFailureError)
                }
            }
        }
    }
    
    /// Get all school SAT Results
    ///
    /// - Parameter completion: sat results or error string
    func getSATResults(completion: @escaping (_ result: [SATResult]?,_ error: String?) -> ()) {
        
        router.request(.getSATResults) { data, response, error in
            guard error == nil else {
                completion(nil, "Please check your network connection.")
                return
            }
            
            if let response = response as? HTTPURLResponse {
                let result = NetworkResponseHandler.handleNetworkResponse(response)
                switch result {
                case .success:
                    guard let responseData = data else {
                        completion(nil, NetworkResponse.noData.rawValue)
                        return
                    }
                    
                    do {
                        let schools = try JSONDecoder().decode([SATResult].self, from: responseData)
                        
                        completion(schools, nil)
                    } catch {
                        #if DEBUG
                        print("GET All SAT Results: ERROR \n \(error)")
                        #endif
                        completion(nil, NetworkResponse.unableToDecode.rawValue)
                    }
                case .failure(let networkFailureError):
                    completion(nil, networkFailureError)
                }
            }
        }
    }
}
